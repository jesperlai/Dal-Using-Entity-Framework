﻿using MySample.DAL.RepoParams;
using MySample.DAL.Service;
using System.Collections.Generic;

namespace MySample.Console
{
    class Program
    {
        static void Main(string[] args)
        {
           // var myService = new SampleService(new SampleRepository("MySampleExe"));
            var myService = new SampleService(new SampleRepositoryMock("MySampleExe"));
            var result = myService.GetTable(new pMySampleTable { NAME = new List<string> { "John", "Mary" } });
        }
    }
}
