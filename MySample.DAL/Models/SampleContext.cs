﻿using MySample.DAL.Extensions.Sample;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;

namespace MySample.DAL.Extensions
{
    public class SampleContext : DbContext
    {
        public DbSet<MySampleTable> MySampleTable { get; set; }
        public SampleContext(string connStr) : base(new SqlConnection(connStr), true)
        {
            Database.SetInitializer<SampleContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
