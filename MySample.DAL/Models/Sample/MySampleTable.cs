﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MySample.DAL.Extensions.Sample
{
    public class MySampleTable
    {
        [Key]
        [Column(Order = 0)]  //當 Table 是複合主鍵時必定義 Order
        public int ID { get; set; }
        public string NAME { get; set; }
        public DateTime BIRTHDAY { get; set; }
        public double HEIGHT { get; set; }
        public double WEIGHT { get; set; }
    }
}
