﻿using System.Collections.Generic;
using System.Text;
using MySample.DAL.Extensions;
using MySample.DAL.Extensions.Sample;

namespace MySample.DAL.RepoParams
{
    public interface ISampleRepository
    {
        SampleContext ContextInit(StringBuilder sb);
        List<MySampleTable> GetTable(pMySampleTable param);
    }
}