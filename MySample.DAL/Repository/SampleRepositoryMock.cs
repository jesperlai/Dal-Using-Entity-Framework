﻿using MySample.DAL.Extensions;
using MySample.DAL.Extensions.Sample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MySample.DAL.RepoParams
{
    public class SampleRepositoryMock : ISampleRepository
    {
        public SampleRepositoryMock(string projName)
        {

        }

        public SampleContext ContextInit(StringBuilder sb)
        {
            return new SampleContext("Mock");
        }

        public List<MySampleTable> GetTable(pMySampleTable param)
        {
            var result = new List<MySampleTable>
            {
                new MySampleTable {  ID = 1, NAME = "John", BIRTHDAY = DateTime.Parse("2001/1/1"), HEIGHT = 180, WEIGHT = 70},
                new MySampleTable {  ID = 2, NAME = "Mary", BIRTHDAY = DateTime.Parse("2002/1/1"), HEIGHT = 170, WEIGHT = 48},
                new MySampleTable {  ID = 3, NAME = "Tom", BIRTHDAY = DateTime.Parse("2003/1/1"), HEIGHT = 190, WEIGHT = 80},
            };

            if (param.NAME.IsValid()) result = result.Where(q => param.NAME.Contains(q.NAME)).ToList();

            return result;
        }
    }
}
