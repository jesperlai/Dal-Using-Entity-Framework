﻿using MySample.DAL.Extensions;
using MySample.DAL.Extensions.Sample;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace MySample.DAL.RepoParams
{
    public class SampleRepository : ISampleRepository
    {
        private string _projName;
        private static readonly int TimeOut = 60;   //通常放在config中

        public SampleRepository(string projName)
        {
            _projName = projName;
        }

        public SampleContext ContextInit(StringBuilder sb)
        {
            //通常連線字串會放在 config 中
            var dbContext = new SampleContext(@"Data Source=資料庫名稱;user id=帳號;password=密碼;Initial Catalog=SampleDb;Application Name={" + _projName + "}");

            //綁定資料庫 timeout 限制秒數
            if (TimeOut > 0) dbContext.Database.CommandTimeout = TimeOut;

            //寫出 Ef 自動產生的 Sql Script
            dbContext.Database.Log = log => sb.Append(log);

            return dbContext;
        }

        /// <summary>
        /// 因為希望錯誤 Log 可以明確指出是哪個 function name 壞掉
        /// </summary>
        [MethodImpl(MethodImplOptions.NoInlining)]
        internal static string GetFuncName()
        {
            return new StackTrace().GetFrame(1).GetMethod().Name;
        }

        public List<MySampleTable> GetTable(pMySampleTable param)
        {
            var sb = new StringBuilder();

            using (SampleContext dbContext = ContextInit(sb))
            {
                IQueryable<MySampleTable> result = dbContext.MySampleTable;

                if (param.NAME.IsValid()) result = result.Where(q => param.NAME.Contains(q.NAME));

                return result.ToListWithNoLock(_projName, GetFuncName(), sb);
            }
        }
    }
}
