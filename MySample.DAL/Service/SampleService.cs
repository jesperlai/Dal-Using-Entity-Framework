﻿using MySample.DAL.Extensions.Sample;
using MySample.DAL.RepoParams;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MySample.DAL.Service
{
    public class SampleService
    {
        ISampleRepository repo;

        public SampleService(ISampleRepository pRepo)
        {
            repo = pRepo;
        }

        public List<MySampleTable> GetTable(pMySampleTable param)
        {
            //移除非法參數
            param = CheckRepoParam(param);

            return IsAllParamNull(param) ? new List<MySampleTable>() : repo.GetTable(param);
        }

        private static T CheckRepoParam<T>(T obj)
        {
            //重定義大家傳入的參數
            obj = RemoveInvalidParam(obj);

            return obj;
        }

        /// <summary>
        /// 移除不合法的查詢參數
        /// </summary>
        private static T RemoveInvalidParam<T>(T obj)
        {
            var props = obj.GetType().GetProperties();
            foreach (var prop in props)
            {
                if (prop.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && prop.PropertyType != typeof(String))
                {
                    var typeArg = prop.PropertyType.GetGenericArguments().First();
                    var type = typeof(List<>).MakeGenericType(typeArg);
                    var newList = (IList)Activator.CreateInstance(type);

                    var value = prop.GetValue(obj);

                    if (value == null) continue;
                    //var result = ((IEnumerable)value).Cast<object>().Where(q => q != null && !string.IsNullOrWhiteSpace(q.ToString())).Distinct();   //如果參數內有 null 是你要移除的
                    var result = ((IEnumerable)value).Cast<object>().Distinct();                                                                       //如果參數內有 null 是你要留著的

                    foreach (var item in result)
                    {
                        newList.Add(item);
                    }

                    prop.SetValue(obj, newList);
                }
            }

            return obj;
        }

        /// <summary>
        /// 是否傳入的參數全為空
        /// </summary>
        private static bool IsAllParamNull<T>(T obj)
        {
            var props = obj.GetType().GetProperties();

            foreach (var prop in props)
            {
                var value = prop.GetValue(obj, null);
                if (value != null) return false;
            }

            return true;
        }
    }
}
