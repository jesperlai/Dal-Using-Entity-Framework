﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;

namespace MySample.DAL.Extensions
{
    internal static class MyExtension
    {
        //通常放在config中
        private static readonly int MaxRecordCount = 50000;

        internal static List<T> ToListWithNoLock<T>(this IQueryable<T> query, string projName, string funcName, StringBuilder sb)
        {
            List<T> result;

            //最多只取出 MaxRecordCount 筆資料
            try
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = IsolationLevel.ReadUncommitted }))
                {
                    result = query.Take(MaxRecordCount).ToList();
                }
            }
            catch (Exception ex)
            {
                ExportEfLog(projName, ("~Error_" + funcName), sb.Append("\n\n" + ex.ToString()));  //若出錯必寫log
                throw ex;
            }

            //補捉各種自訂錯誤 (ex: 超過50000筆)
            ErrorHandler(projName, funcName, sb, result);

            // SQL → TXT
            ExportEfLog(projName, funcName, sb);

            // Data → CSV
            ExportEfResult(projName, funcName, result);

            sb = null;

            return result;
        }

        /// <summary>
        /// SQL 查詢字串匯出為 txt 檔
        /// </summary>
        private static void ExportEfLog(string projName, string funcName, StringBuilder sb)
        {
            string fullPath = CreateDirAndGetFullPath(projName, funcName, "txt");

            try
            {
                File.WriteAllText(fullPath, sb.ToString(), new UTF8Encoding(true));
            }
            catch
            {
                //do something
            }
            finally
            {
                sb.Clear();
            }
        }

        /// <summary>
        /// 若資料夾不存在則創建資料夾，並且回傳完整檔案路徑
        /// </summary>
        private static string CreateDirAndGetFullPath(string projName, string funcName, string format)
        {
            DateTime dt = DateTime.Now;

            string dir = @"D:\SQL_ERROR\" + projName + @"\" + dt.ToString("yyyyMMdd") + @"\";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            string fileName = funcName + "_" + dt.ToString("yyyyMMdd_HHmmss_fffffff") + "." + format;

            return (dir + fileName);
        }

        /// <summary>
        /// SQL 查詢結果匯出為 csv 檔
        /// </summary>
        internal static void ExportEfResult<T>(string projName, string funcName, List<T> obj)
        {
            string fullPath = CreateDirAndGetFullPath(projName, funcName, "csv");

            //針對時間格式做格式化
            JsConfig<DateTime>.SerializeFn = time => new DateTime(time.Ticks).ToString("yyyy/MM/dd HH:mm:ss");

            try
            {
                //使用 Service Stack 來幫我們處理 List → csv 的序列化
                var data = CsvSerializer.SerializeToCsv<T>(obj);
                File.WriteAllText(fullPath, data, new UTF8Encoding(true));
            }
            catch
            {

            }
        }

        private static void ErrorHandler<T>(string projName, string funcName, StringBuilder sb, List<T> result)
        {
            if (result.Count >= MaxRecordCount)
            {
                ExportEfLog(projName, "~OverCountLimit_" + funcName, sb);  //出錯必寫log
                throw new OutOfMemoryException("取出資料筆數，超過上限" + MaxRecordCount.ToString() + "筆");
            }
        }

        internal static bool IsValid<T>(this IEnumerable<T> param)
        {
            return param != null;
        }
    }
}
