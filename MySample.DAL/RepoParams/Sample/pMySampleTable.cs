﻿using System.Collections.Generic;

namespace MySample.DAL.RepoParams
{
    /// <summary>
    /// 當未來有第2、3、4...個參數時 只要修改 Repository 即可 不影響外部程式叫用
    /// </summary>
    public class pMySampleTable
    {
        public IEnumerable<string> NAME { get; set; }
    }
}
