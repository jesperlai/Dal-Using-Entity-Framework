# 目的 #
1. Repository 使用 EF 操作 DB
<br /><br />
2. Service 為外呼叫的對象 (並且要對送進來的參數做一些`客製化的檢查`，確認沒問題才送進 Repository 使用) ... 會用到一些 Reflection
<br /><br />
3. 希望 DAL 是由誰呼叫 可以在 DB 端留下記錄 (下 `sp who` 可以看到 你的 program name)
<br />
![sp_who2](https://az787680.vo.msecnd.net/user/jesper/33b2b452-d5cc-4d5d-9069-4a462ebbfc94/1521689117_79051.png "sp_who2")
<br /><br />
4. 有些客制化的 DB 存取限制 (例如: `單次取出上限 50000 筆、超過 60 秒算 timeout` )
<br /><br />
5. 當發生 Exception 時 `自動記錄Log` (包含 Sql Script 與資料內容) ... 我使用 Service Stack 將資料內容記錄成 csv 檔
<br />
![異常log](https://az787680.vo.msecnd.net/user/jesper/33b2b452-d5cc-4d5d-9069-4a462ebbfc94/1521689930_90455.png "異常log")
<br /><br />
6. 希望取資料時是 `with (nolock)`
<br /><br />
7. 希望未來同一個 function 查詢`參數增加`時 不要影響現有舊程式的運作